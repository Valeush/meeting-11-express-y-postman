const express = require('express');
const morgan = require ('morgan');
const server = express();
let arr = ['cero' ,'uno', 'dos', 'tres'];

server.use(morgan('dev'));

server.get ('/', (req , res) =>{
    res.send('App web desarrollada en meeting 11 de acamica');
})

server.get ('/phones/', (req , res) =>{
    res.json(arr);
})


server.get ('/phones/:id', (req , res) =>{
    let id = req.params.id;
    res.json(arr[id]);
})

server.post ('/phones', (req , res) =>{
   // let id = req.params.id;
   // arr.splice(id, 1)
    console.log(req);
    res.json({resultado: "opcion sin implementar"});
})

server.delete ('/phones/:id', (req , res) =>{
    let id = req.params.id;
    arr.splice(id, 1);
    console.log(id, arr);
    res.json(arr);
})


server.get ('/version', (req , res) =>{
    res.json({version: "1.0.0"});
})

server.get ('/time', (req , res) =>{
    let t = new Date();
    res.json(t);
})

let port = 3000;
server.listen (port, () =>{
    console.log('Start server...' + port);
    console.log(`Start server...${port}`);
})

